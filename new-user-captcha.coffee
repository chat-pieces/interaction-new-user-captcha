{ timeout } = require 'small-simple-bot/helpers/timing'

helloMessages = {}

module.exports = ({chatId, timeoutSecs, options, welcomeTmpl, failTmpl, successTmpl})->

    timeoutSecs ?= 60

    options ?=
        'heart': '❤️'
        'poop' : '💩'
        'ghost': '👻'
        'cat'  : '😺'
        'baby' : '👶'

    emojiOk = '👍' # nunca será uma opção válida. E é um bom honeypot.

    welcomeTmpl ?= '''
        <b>Hello <a href="%USER_URI%">%USER_NAME%</a>!</b>
        
        If you'r human, please, select the %OPT%.
        
        You have %SECS% seconds.'''

    failTmpl ?= '<a href="%USER_URI%">%USER_NAME%</a> fail to answer the captcha.'

    successTmpl ?= 'Wellcome <a href="%USER_URI%">%USER_NAME%</a>!'

    buildTmpl = (tmpl, vars)->
        for key, val of vars
            tmpl = tmpl.replace ///%#{key}%///gsm, val
        tmpl

    optKeys = Object.keys(options)

    cbPrefix = 'usrcaptcha' + chatId

    humanTestHello = (bot, act)->
        throw Error "#{act.chat.id} isnt the protected #{chatId}." if act.chat.id isnt chatId
        return if act.minutesAgo > 2 # testing only here, so humanTestClick can test after a reboot.
        newUser = act.new_chat_participant
        msgId = act.message_id
        name = (newUser.first_name or newUser.username).replace(/\s.*/,'').replace(/[<>&]/g,' ')
        name = name.substring 0, 10 # size limit
        opt = optKeys[ Math.floor( Math.random() * optKeys.length ) ]
        helloKey = Math.random().toString(36).substring(2,8)
        helloMessages[helloKey] = { }
        keyboard = JSON.stringify inline_keyboard: [[
            (for key, emoji of options
                ok = if key is opt then 'OK' else 'FAIL' + key
                {
                    text: emoji
                    callback_data: "#{cbPrefix}##{newUser.id}##{ok}##{name}##{helloKey}"
                }
            )...
            {
                text: emojiOk
                callback_data: "#{cbPrefix}##{newUser.id}#FAIL##{name}##{helloKey}"
            }
        ]]
        timeoutCaptcha = timeoutSecs
        helloMsg = ()-> buildTmpl welcomeTmpl, {
            USER_URI: "tg://user?id=#{newUser.id}"
            USER_NAME: name
            OPT: opt
            SECS: timeoutCaptcha
        }
        msgConf = reply_to_message_id: msgId, parse_mode: 'HTML', reply_markup: keyboard
        bot.sendMessage helloMsg(), chatId, msgConf, (error, response, result)->
            msgId = result?.message_id
            timeout timeoutSecs + 60*30, -> bot.deleteMessage chatId, msgId
            return if error or not msgId
            countdown = bot.helpers.timing.interval 10, ->
                timeoutCaptcha -= 10
                if timeoutCaptcha > 0
                    bot.editMessageText helloMsg(), chatId, msgId, msgConf
                else
                    clearTimeout countdown
                    msg = buildTmpl failTmpl, USER_URI: newUser.id, USER_NAME: name
                    bot.editMessageText msg, chatId, msgId, parse_mode: 'HTML'
                    untilDate = 60*60 + Math.round Date.now() / 1000 # uma hora
                    bot.kickChatMember chatId, newUser.id, untilDate
                    delete helloMessages[helloKey]
            helloMessages[helloKey].countdown = countdown

    humanTestClick = (bot, act)->
        throw Error "#{act.chat.id} isnt the protected #{chatId}." if act.chat.id isnt chatId
        msgId = act.message.message_id
        data = act.data.split '#'
        newUsrId = data[1]
        isOK = data[2] is 'OK'
        name = data[3]
        helloKey = data[4]
        if act.from.id.toString() is newUsrId or bot.isAdm act.from.username
            if isOK
                if helloMessages[helloKey]?.countdown
                    clearInterval helloMessages[helloKey].countdown
                    delete helloMessages[helloKey]
                msg = buildTmpl successTmpl, USER_URI: newUsrId, USER_NAME: name
                bot.editMessageText msg, chatId, msgId, parse_mode: 'HTML'
        else
            clicker = act.from.username or act.from.first_name
            bot.logger.log "#{clicker} tries to validate #{name}'s humanTest."

    newUserCaptcha = (bot, update)->
        act = update._act
        return if act.chat.id isnt chatId
        if act?.new_chat_participant
            humanTestHello bot, act
        if update._kind is 'callback_query' and act.data[..cbPrefix.length-1] is cbPrefix
            humanTestClick bot, act

    newUserCaptcha.ignoreTimeout = true
    return newUserCaptcha

